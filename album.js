class Album {
	constructor (name, volume) {
		this.name = name
		this.volume = volume
		this.tracks = []
	}

	add_track (t) {
		this.tracks.push(t)
	}

	artist () {
		if (this._artist) return this._artist

		let artist
		for (let i in this.tracks) {
			if (this.tracks[i].artist && ['various', 'va'].indexOf(this.tracks[i].artist.toLowerCase()) > -1) return this._artist = 'va'

			if(artist){
				if (this.tracks[i].artist !== artist) {
					return this._artist = 'va'
				}
			} else {
				artist = this.tracks[i].artist
			}
		}
		return this._artist = artist
	}

	year () {
		const year = this.tracks.filter(t => !!t.albumdate)?.[0].albumdate

		switch (typeof year) {
			case 'string': {
				const y = year.split('-')[0]
				if (y !== '0000') return y
				break
			}
			case 'object':
				return year.getFullYear()
		}
	}

	format () {
		const filename = this.tracks[0].filename.split('.')
		if (filename.length > 1) {
			return filename.pop()
		}
	}

	render () {
		return {
			name: this.name,
			artist: this.artist().decodeHTML(),
			volume: this.volume,
			thumb: this._to_filename(this.artist()) + '-' + this._to_filename(this.name) + '.jpg',
			tracks: this.tracks.sort((a, b) => a.songnb > b.songnb ? 1 : -1)
		}
	}

	_to_filename (s) {
		const translations = {
			" ": "_",
			".": "",
			",": "",
			"!": "",
			"/": "",
			"&amp;": "",
			"&": "",
			"#": "",
			";": "",
			":": "",
			'"': "",
			'%': "",
			"&#233;": "e",
			// japanese utf-8 - see http://utf8-chartable.de/unicode-utf8-table.pl
			"%E3%81%95": "_uE38195",
			"%E3%82%93": "_uE38293",
			"?": "_uE38293",
		}

		return s.translate(translations).toLowerCase()
	}
}

String.prototype.translate = function(translation) {
  const names = []
  for (let search in translation) {
    if (translation.hasOwnProperty(search)) {
      names.push(search.replace(/([.\\+*?[^\]$(){}=!<>|:-])/g, "\\$1"))
    }
  }

  const re = new RegExp("(?:" + names.join("|") + ")", "g")
  return this.replace(re, val => translation[val])
}

if (!String.prototype.decodeHTML) {
  String.prototype.decodeHTML = function () {
    return this
      .replace(/&amp;/g, '&')
      .replace(/&#233;/g, 'E')
  }
}

module.exports = Album
