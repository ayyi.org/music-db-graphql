const { readFileSync } = require('node:fs')
const { ApolloServer } = require('apollo-server')
const { responseCachePlugin, LogCacheDirective, PurgeCacheDirective, } = require('@ayyi/apollo-response-cache')
const Database = require('./db')
const Logger = require('./logger')
const resolvers = require('./resolvers')
const config = require('./config')
const typeDefs = readFileSync('./schema.graphql', 'utf8')

const idResolver = (type, result) => {
  if(type === 'Album')
    return result.album_id

  if (['Node', 'Response'].indexOf(type) >= 0) {
    return result.__unusual_id__
  }
  return result.id
}

const db_options = config.db.type === 'sqlite3' ? {
  useNullAsDefault: true
} : {};

new ApolloServer(
  {
    typeDefs,
    resolvers,
    schemaDirectives: {
      logCache: LogCacheDirective({idResolver}),
      purgeCache: PurgeCacheDirective({idResolver}),
    },
    cacheControl: {
      defaultMaxAge: 1000
    },
    cors: true,
    dataSources: () => ({
      db: new Database(require('knex')({
        ...db_options,
        client: config.db.type,
        connection: config.db
      }))
    }),
    plugins: [
      responseCachePlugin({ nodeFQCTTL: 100000 }),
      Logger
    ],
    subscriptions: {
      path: '/subscriptions',
      onConnect: (_connectionParams, _webSocket, _context) => {
      },
      onDisconnect: (_webSocket, _context) => {
      },
    }
  })
  .listen({ host: config.host, port: 4000 })
  .then(({ url }) => console.log(`Server ready at ${url}`))
