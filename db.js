/*
 *  knex api details: https://knexjs.org/
 *
 *  Note that the Knex caching is not used because the queries are cached by apollo server
 */
const { SQLDataSource } = require('datasource-sql')

class Database extends SQLDataSource {

  _date () {
    return this.knex.context.client.config.client === 'sqlite3' ? 'albumdate' : 'DATE_FORMAT(albumdate, "%Y")'
  }

  // In many cases it would be faster to do a join here
  // SELECT songname, album.album_id, info from mp3main, album WHERE mp3main.album_id = album.album_id;
  // however this is overfetching in some cases
  async tracks (search = '', limit = 500) {

    function where() {
      return parseInt(search)
        ? this.where('volume', ('00' + search).slice(-3))
        : this.where('songname', 'like', `%${search}%`)
          .orWhere('artist', 'like', `%${search}%`)
          .orWhere('album', 'like', `%${search}%`)
    }

    return where.apply(
        this.knex('mp3main')
          .select(this.knex.raw(`*, songname as name, album as album_name, ${this._date()} as year`))
      )
      .limit(limit)
      .orderBy(['album_id', 'songnb'])
  }

  track (id) {
    return this.knex
      .select(this.knex.raw(this.type === 'sqlite3'
        ? `*, songname as name, album as album_name, albumdate as year`
        : `*, songname as name, album as album_name, ${this._date()} as year`
      ))
      .from('mp3main')
      .where('id', id)
      .then(result => result[0])
  }

  tracks_for_album (album_id) {
    return this.knex('mp3main')
      .select(this.knex.raw(`*, songname as name, album as album_name, ${this._date()} as year`))
      .where('album_id', album_id)
      .orderBy('songnb')
  }

  async albums (id, limit = 10) {

    return id
      ? this.knex('album')
        .select('*')
        .where('album_id', id)
      : this.knex('album')
        .select('*')
        .limit(limit)
  }

  album (id) {
    return this.knex
      .select('*')
      .from('album')
      .where('album_id', id)
      .then(result => result[0])
  }

  volumes (first) {
    const f = (first || '001').slice(0, 3)

    const vol_name = i => (+i).toString().padStart(3, '0')

    return this.knex
      .distinct('volume as id')
      .from('mp3main')
      .where('volume', 'like', f + '%')
      .orWhere('volume', vol_name(+f + 1))
      .orWhere('volume', vol_name(+f + 2))
      .orWhere('volume', vol_name(+f + 3))
      .limit(32)
      //.offset(590)
      .orderBy('volume')
  }

  /*
  volume (id) {
    return this.knex
      .select('*')
      .from('mp3main')
      .where('volume', id)
  }
  */

  volume_count () {
    return this.knex('mp3main')
      .count(this.knex.raw('distinct volume'))
      .then(result => Object.entries(result[0])[0][1])
  }

  album_count () {
    return this.knex('mp3main')
      .count('album')
      .then(result => Object.entries(result[0])[0][1])
  }

  album_ids_by_volume (id) {
    return this.knex
      .select(this.knex.raw('distinct mp3main.album_id,album.name,album.artist from mp3main INNER JOIN album ON mp3main.album_id = album.album_id'))
      .where('volume', id)
  }

  latestVolumes () {
    return this.knex
      .distinct('volume as id')
      .from('mp3main')
      .where(this.knex.raw('volume=(select (MAX(volume) - 4) from mp3main)'))
      .orWhere(this.knex.raw('volume=(select (MAX(volume) - 3) from mp3main)'))
      .orWhere(this.knex.raw('volume=(select (MAX(volume) - 2) from mp3main)'))
      .orWhere(this.knex.raw('volume=(select (MAX(volume) - 1) from mp3main)'))
      .orWhere(this.knex.raw('volume=(select (MAX(volume)) from mp3main)'))
      .limit(5)
      .orderBy('volume')
  }

  /*
   *  Return 1 album for the given volume
   */
  album_by_volume (volume_name) {
    return this.knex
      .select(this.knex.raw('album.album_id, album.artist, album.name'))
      .from(this.knex.raw('album, mp3main'))
      .where(this.knex.raw('volume="' + volume_name + '" AND mp3main.album_id = album.album_id'))
      .limit(1)
      .then(result => result[0])
  }

  /*
  album_tracks_by_volume (volume_name) {
    return this.knex('mp3main')
      .select('artist', 'songname', 'album')
      .where('volume', volume_name)
      .limit(20)
  }
  */

  stats () {
    return this.knex('mp3main')
      .select(this.knex.raw('genre as name, count(genre) as count'))
      .whereNot('genre', '')
      .whereNot('genre', 'Other')
      .whereNot('genre', '255')
      .groupBy('genre')
      .orderBy('count', 'desc')
      .limit(10)
  }

  //
  //
  // Mutations
  //
  //

  async add_track (input, album) {
    // TODO return the track, not just the id - can we let the resolver do it?
    if (!input.volume) throw 'Volume missing'
    if (!input.album) throw 'Album missing'

    if (this.knex.context.client.config.client === 'sqlite3') {
      input.albumdate = input.albumdate.getFullYear() + '-01-01'
    }

    input.songname = input.name
    delete input.name

    const _add_album = async () => {
      let { album_id } = input

      if (!album_id) {
        album_id = (await this.knex('mp3main')
          .distinct('album_id')
          .where('volume', input.volume)
          .where('album', input.album))[0]?.album_id

        if (!album_id) {
          album_id = await this.insert_album(input.album, album.artist, input.volume)
        }
      }

      return album_id
    }

    if (!input.album_id) {
      input.album_id = await _add_album()
    }

    return this.knex('mp3main')
      .insert(input)
      .then(
        async a => ({id: a[0], album_id: input.album_id})
      )
  }

  async delete_track (id) {
    if(!(id > 0)) throw `bad id ${id}`

    const track = await this.track(id)
    const album_id = track.album_id

    const ret = this.knex('mp3main')
      .where('id', id)
      .del()
      .then(res => (res === 1) && track)

    if(album_id){
      const count = Object.entries((await this.knex('mp3main')
        .count()
        .where('album_id', album_id))[0])[0][1]

      if(count < 1){
        console.log(`db.delete_track: album_id ${album_id} has no references - TODO delete`)
      }
    }

    return ret
  }

  set_album_artist (album_id, artist) {
    return this.knex
      .update('artist', artist)
      .from('album')
      .where('album_id', album_id)
  }

  // TODO update 'album' for each track with this album_id
  set_album_name (album_id, name) {
    return this.knex
      .update('name', name)
      .from('album')
      .where('album_id', album_id)
  }

  set_album_year (album_id, year) {
    return this.knex
      .update('albumdate', year)
      .from('mp3main')
      .where('album_id', album_id)
  }

  set_album_volume (album_id, volume) {
    return this.knex
      .update('volume', volume)
      .from('mp3main')
      .where('album_id', album_id)
  }

  set_track_artist (id, artist) {
    return this.knex
      .update('artist', artist)
      .from('mp3main')
      .where('id', id)
  }

  set_track_name (id, name) {
    return this.knex
      .update('songname', name)
      .from('mp3main')
      .where('id', id)
  }

  async delete_album_album (id) {
    const album = await this.album(id)

    return this.knex('album')
      .where('album_id', id)
      .del()
      .then(res => res === 1 ? album : null)
  }

  //-------------------------------------------

  /*
   *  Returns the id of the new row
   */
  insert_album (name, artist, volume) {
    if (!volume) throw('missing volume name')

    return this.knex
      .insert([{ name, artist }])
      .into('album')
      .then(async r => {
        await this.knex
          .update('album_id', r[0])
          .from('mp3main')
          .where({volume, album: name})
        return r[0]
      })
  }

  set_album_for_tracks () {
  }

  //-------------------------------------------

  // admin

  album_orphans (limit = 10) {
    return this.knex
      .select('*')
      .from('album')
      .where(this.knex.raw('NOT EXISTS (SELECT album_id FROM mp3main WHERE album.album_id = mp3main.album_id) LIMIT ' + limit))
  }

  no_album (volume, limit = 10) {
    return this.knex
      .select('*', 'songname as name')
      .from('mp3main')
      .where('album_id', 'NULL')
      .orWhere('album_id', null)
      .orWhere('album_id', 0)
      .where(volume ? {volume: volume} : this.knex.raw('TRUE'))
      .limit(limit)
  }

  /*
   *   Create a new album for the given track
   */
  async fix_album_album (track_id) {
    const track = (await this.knex
      .select('volume', 'album', 'artist', 'filedir')
      .from('mp3main')
      .where('id', track_id))[0]

    const find_album_from_path = track => {
      if (track.filedir) {
        const dir = track.filedir.split('/').pop();
        if (dir) {
          return dir.split(' - ')
        }
      }
      else return []
    }

    let artist = track.artist
    let albumname = track.album
    if (!track.album || !track.artist) {
      const [_artist, _album_name] = find_album_from_path(track)
      if (_artist && _album_name) {
        artist = _artist
        albumname = _album_name
      } else {
        throw `Unable to create album (cannot find album name from path "${track.filedir}")`
      }
    }

    const tracks = await this.knex
      .select('id', 'album', 'album_id', 'artist')
      .from('mp3main')
      .where('album', track.album)
      .where('volume', track.volume)
    if (!tracks) {
      throw 'No tracks selected for update'
    }

    const have_album = tracks.filter(v => v.album_id !== 0 && v.album_id !== null).length
    if (have_album) throw Error('tracks already have album_id ' + tracks.filter(v => !!v.album_id)[0].album_id)

    artist = tracks.reduce(
      (acc, t) => {
         if (t.artist && acc && t.artist !== acc) return 'va'
         return acc
      },
      artist
    )

    if (albumname && track.volume && artist) {
      if (!track.album) {
        tracks.forEach(async track => {
          if (!track.album)
            await this.knex.update('album', albumname).from('mp3main').where('id', track.id)
          if (!track.artist)
            await this.knex.update('artist', artist).from('mp3main').where('id', track.id)
        })
      }

      await this.insert_album (albumname, artist, track.volume)
      return tracks.map(({ id }) => ({ id }))
    } else {
      if (!artist)
        throw 'Unable to create album (artist missing)'
      if (!albumname)
        throw 'Unable to create album (album name missing)'
      else
        throw 'Unable to create album'
    }
  }
}

module.exports = Database
