music-db-graphql
----------------

![Test Status](https://gitlab.com/ayyi.org/music-db-graphql/badges/master/pipeline.svg)
![coverage](https://gitlab.com/ayyi.org/music-db-graphql/badges/master/coverage.svg)

music-db-graphql is a music database GraphQL frontend.

It is based on [Apollo server](https://github.com/apollographql/apollo-server)

Adding a GraphQL frontend in front of the database allows the data to be more
easily consumed by clients. It increases performance by adding a caching layer.
Because the cache stores the data in the exact format expected by the clients,
this decoupling allows the database implementation to be freed from performance
constraints.

![Screenshot](https://gitlab.com/ayyi.org/music-db-graphql/-/raw/master/screenshot.webp "Screenshot")

An example GraphQL query would be
```
query {
  tracks(search: "monkey") {
    name
    artist
    album
    year
  }
}
```

A very over-simplified schema looks like:

```graphql
const typeDefs = gql`
  type Track {
    id: Int!
    songnb: Int
    name: String
    album_name: String
    artist: String
    volume: String
    min: Int
    sec: Int
    year: String
    album(id: Int): Album
  }

  type Album {
    album_id: Int!
    name: String
    artist: String
    tracks: [Track]
  }

  input TrackInput {
    name: String!
    artist: String
    album: String
    songnb: Int
    volume: String
    year: String
    album_id: Int
  }

  type Query {
    tracks(search: String, id: Int, limit: Int): [Track]
  }

  type Mutation {
    addTrack(input: TrackInput): Track
    deleteTrack(id: Int): Track
  }
```
[See full schema](schema.js)


Installation
------------

```
npm i @ayyi/music-db-graphql
```

Create the file config.yaml:
```
db:
  type: mysql
  host: 10.0.0.10
  user: myuser
  password: mypassword
  database: mydatabase
```
```
db:
  type: sqlite
  filename: ./mydb.sqlite
```
Type should be one of `pg` (PostgreSQL and Amazon Redshift), `mysql` (MySQL or MariaDB), `sqlite3`, or `mssql`.


Test server
-----------

To run a test server with fixture data:
```
node test-server.js
```
