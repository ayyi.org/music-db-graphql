const { parseResolveInfo } = require('graphql-parse-resolve-info')
const { PubSub } = require('apollo-server')
const { ApolloError } = require('apollo-server-errors')
const Album = require('./album')
const Scan = require('./scan')

const pubsub = new PubSub()
pubsub.asyncIterator(['FILE_ADDED', 'ALBUM_UPDATED', 'VOLUME_UPDATED', 'FILE_REMOVED', 'ALBUM_REMOVED'])

const resolvers = {
  Album: {
    tracks: async (parent, _args, { dataSources, tracks }) => {
      return tracks?.[parent.album_id] || await dataSources.db.tracks_for_album(parent.album_id)
    },
    format: (parent, _args, { tracks }) => {
      const t = tracks?.[parent.album_id]
      return t && t.length && t[0].filename.split('.').pop()
    },
    thumbnail: (parent, _args, { tracks }) => {
      const artist = parent.artist || tracks[parent.album_id]?.[0]?.artist
      if (artist) {
        const album = new Album(parent.name, 0)
        album.add_track({artist})
        return album.render().thumb
      }
    }
  },
  Track: {
    albumObj: async (parent, _args, { dataSources }) =>
      dataSources.db.album(parent.album_id)
  },
  Volume: {
    thumbnail: async (parent, _args, { dataSources: { db } }) => {
      let album = await db.album_by_volume(parent.id)
      /*
      if (!album) {
        // track is missing entry in albums table. create it.
        const fill_at = 599
        if (parent.name === '' + fill_at || parent.name === '' + (fill_at + 1) || parent.name === '' + (fill_at + 2) || parent.name === '' + (fill_at + 3)) {
          const tracks = await db.album_tracks_by_volume(parent.name)
          const track = tracks[0]
          const artist = tracks.reduce(
            (acc, t) => {
              if (t.album === track.album) {
                if (t.artist !== acc) return 'va'
              }
              return acc
            },
            track.artist
          )

          album = {
            album_id: await db.insert_album(track.album, artist, parent.name),
            name: track.album,
            artist
          }
        }
      }
      */

      if (album) {
        if (album.artist && album.name) {
          const a = new Album(album.name, parent.name)
          a.add_track({artist: album.artist})
          return a.render().thumb
        }
      }
      return null
    }
  },
  Query: {
    tracks: async (_source, { search, id, limit }, { dataSources }) => {
      if (id) {
        const a = await dataSources.db.track(id)
        return a ? [a] : []
      }

      return dataSources.db.tracks(search, limit)
    },
    albums: async (_source, { id, limit }, context, info) => {
      const { dataSources } = context

      const albums = await dataSources.db.albums(id, limit)

      const fields = parseResolveInfo(info).fieldsByTypeName.AlbumsConnection.albums.fieldsByTypeName.Album
      // The track list has to be loaded early as it is needed by the 'format' resolver
      if (fields.format || fields.thumbnail) {
        context.tracks = await albums.reduce(
          async (_acc, v) => {
            const acc = await _acc
            acc[v.album_id] = await dataSources.db.tracks_for_album(v.album_id)
            return acc
          },
          {}
        )
      }

      return {
        pager: { count: dataSources.db.album_count() },
        albums
      }
    },
    volumes: async (_source, { first, id }, { dataSources }) => {
      if (id) {
        return {
          volumes: [{
            id,
            albums: dataSources.db.album_ids_by_volume(id)
          }]
        }
      }

      return {
        pager: { count: dataSources.db.volume_count() },
        volumes: await dataSources.db.volumes(first)
      }
    },
    latestVolumes: (_source, _args, { dataSources }) => {
      return dataSources.db.latestVolumes()
    },
    scan: async (_source, { id }) => {
      const albums = await Scan.albums(id)
        .catch(e => {throw new ApolloError('' + e, e.code, {})})
        || []

      return { albums, inProgress: true }
    },
    stats: async (_source, _, { dataSources }) => {
      const genres = await dataSources.db.stats()
      return { genres }
    },
    albumOrphans: async (_source, { limit }, { dataSources }) => {
      return dataSources.db.album_orphans(limit)
    },
    tracksWithoutAlbum: async (_source, { volume, limit }, { dataSources }) => {
      return dataSources.db.no_album(volume, limit)
    }
  },
  Mutation: {
    addTrack: async (_, { input }, { dataSources }) => {
      input.albumdate = new Date(input.albumdate, 0)
      return dataSources.db.add_track(input, {artist: input.artist})
    },

    deleteTrack: (_, { id }, { dataSources }) =>
      dataSources.db.delete_track(id)
    ,

    updateAlbumArtist: async (_, { album_id, artist }, { dataSources }) => {
      const response = await dataSources.db.set_album_artist(album_id, artist)
      if (response < 1) console.warn('no rows changed')

      return dataSources.db.album(album_id)
    },

    updateAlbumName: async (_, { album_id, name }, { dataSources }) => {
      const response = await dataSources.db.set_album_name(album_id, name)
      if (response < 1) console.warn('no rows changed')

      return dataSources.db.album(album_id)
    },

    updateAlbumYear: async (_, { album_id, year }, { dataSources }) => {
      const response = await dataSources.db.set_album_year(album_id, year)
      if (response < 1) console.warn('no rows changed')

      return dataSources.db.album(album_id)
    },

    updateAlbumVolume: async (_, { album_id, volume }, { dataSources }) => {
      const response = await dataSources.db.set_album_volume(album_id, volume)
      if (response < 1) console.warn('no rows changed')

      return dataSources.db.album(album_id)
    },

    updateTrackArtist: async (_, { id, artist }, { dataSources }) => {
      const response = await dataSources.db.set_track_artist(id, artist)
      if (response !== 1) console.warn('updateTrackArtist: unexpected response', response)

      return dataSources.db.track(id)
    },

    updateTrackName: async (_, { id, name }, { dataSources }) => {
      const response = await dataSources.db.set_track_name(id, name)
      if (!response) console.warn('updateTrackArtist: unexpected response', response)

      return dataSources.db.track(id)
    },

    deleteAlbumAlbum: async (_source, { id }, { dataSources }) => {
      return dataSources.db.delete_album_album(id)
    },

    fixAlbumAlbum: async (_source, { id }, { dataSources }) => {
      return dataSources.db.fix_album_album(id)
    },

    import: async (_source, { id }, { dataSources }) => {
      await Scan.import(dataSources.db, id)
      pubsub.publish('VOLUME_UPDATED', { volumeUpdated: id })
      return { id }
    },

    /*
     *  Only for testing
     */
    addAlbum: async (_source, { album }, { dataSources }) => {
      return dataSources.db.insert_album (album.name, album.artist, album.volume)
    },
  },
  Subscription: {
    fileAdded: {
      subscribe: () => pubsub.asyncIterator(['FILE_ADDED'])
    },
    albumUpdated: {
      subscribe: () => pubsub.asyncIterator(['ALBUM_UPDATED'])
    },
    volumeUpdated: {
      subscribe: () => pubsub.asyncIterator(['VOLUME_UPDATED'])
    },
    fileRemoved: {
      subscribe: () => pubsub.asyncIterator(['FILE_REMOVED'])
    },
    albumRemoved: {
      subscribe: () => pubsub.asyncIterator(['ALBUM_REMOVED'])
    },
  }
}

Scan.track_added.subscribe(track => {
  pubsub.publish('FILE_ADDED', {
    fileAdded: track
  })
})

Scan.track_removed.subscribe(track => {
  pubsub.publish('FILE_REMOVED', {
    fileRemoved: { id: track.id, album_id: track.album_id }
  })
})

Scan.album_update.subscribe(albumUpdated => {
  pubsub.publish('ALBUM_UPDATED', { albumUpdated })
})

Scan.album_removed.subscribe(albumRemoved => {
  pubsub.publish('ALBUM_REMOVED', { albumRemoved })
})

module.exports = resolvers
