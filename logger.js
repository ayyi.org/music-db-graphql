/*
 *  https://www.apollographql.com/docs/apollo-server/integrations/plugins/
 *
 *  Available lifecycle hooks:
 *    serverWillStart
 *    requestDidStart
 *
 *  Each request:
 *    parsingDidStart
 *    validationDidStart
 *    didResolveOperation
 *    responseForOperation
 *    executionDidStart
 *    didEncounterErrors
 *    willSendResponse
 *
 *
 *  lru-cache (least recently used):
 *    https://www.npmjs.com/package/lru-cache
 */

const show_cache = cache => {
  cache.store.length
    ? cache.store.forEach((v, k) => {
      let data = v
      try {
        data = JSON.parse(v).data
      } catch(e) {}
      console.log(' *', k, data)
    })
    : console.log('cache EMPTY')
}

const myPlugin = {

  // Fires whenever a GraphQL request is received from a client.
  requestDidStart(requestContext) {
    console.log('Request start:\n' + requestContext.request.query)
    show_cache(requestContext.cache)
    console.log('')

    return {

      // Fires whenever Apollo Server will parse a GraphQL
      // request to create its associated document AST.
      parsingDidStart (_requestContext) {
        //console.log('Parsing started')
      },

      // Fires whenever Apollo Server will validate a
      // request's document AST against your GraphQL schema.
      validationDidStart (_requestContext) {
        //console.log('Validation started')
      },

      willSendResponse (requestContext) {
        console.log('request END')
        show_cache(requestContext.cache)
        console.log('')
      }
    }
  }
}

module.exports = myPlugin
