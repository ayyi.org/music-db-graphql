const { createTestClient } = require('apollo-server-testing')
const gql = require('graphql-tag')
const { constructTestServer, stopTestServer } = require('./__utils')
const resolvers = require('../resolvers')

let DB
let SERVER

const TRACK = gql`
  query ($id: Int) {
    tracks (id: $id) {
      id
      name
      artist
      albumObj {
        album_id
        thumbnail
      }
    }
  }
`

const TRACKS = gql`
  query {
    tracks (limit: 1) {
      id
      artist
      albumObj {
        album_id
        thumbnail
      }
    }
  }
`

const ALBUM = gql`
  query($id: Int) {
    albums (id: $id) {
      albums {
        album_id
        name
        artist
        thumbnail
      }
    }
  }
`

const ALBUM_WITH_FORMAT = gql`
  query {
    albums (id: 1) {
      albums {
        album_id
        format
      }
    }
  }
`

const VOLUME = gql`
  query($id: ID) {
    volumes (id: $id) {
      volumes {
        albums {
          album_id
        }
        thumbnail
      }
    }
  }
`

const ALBUM_ORPHANS = gql`
  query {
    albumOrphans {
      album_id
    }
  }
`

const TRACKS_WITHOUT_ALBUM = gql`
  query($volume: String) {
    tracksWithoutAlbum(volume: $volume) {
      id
    }
  }
`

const FIX_ALBUM_ALBUM = gql`
  mutation fixAlbumAlbum($id: Int) {
    fixAlbumAlbum(id: $id) { id }
  }
`

const STATS = gql`
  query {
    stats {
      genres {
        name
      }
    }
  }
`

const ADD_TRACK = gql`
  mutation ($input: TrackInput) {
    addTrack(input: $input) {
      id
    }
  }
`

const DELETE_TRACK = gql`
  mutation ($id: Int) {
    deleteTrack(id: $id) {
      id
    }
  }
`

const ADD_ALBUM = gql`
  mutation ($album: AlbumInput) {
    addAlbum(album: $album)
  }
`

const DELETE_ALBUM = gql`
  mutation ($id: Int) {
    deleteAlbumAlbum(id: $id) {
      album_id
    }
  }
`

const SET_ALBUM_NAME = gql`
  mutation updateAlbumName($id: Int!, $name: String!) {
    updateAlbumName(album_id: $id, name: $name) {
      name
    }
  }
`

const SET_ALBUM_ARTIST = gql`
  mutation updateAlbumArtist($id: Int!, $artist: String!) {
    updateAlbumArtist(album_id: $id, artist: $artist) {
      name
    }
  }
`

const SET_ALBUM_YEAR = gql`
  mutation updateAlbumYear($id: Int!, $year: String!) {
    updateAlbumYear(album_id: $id, year: $year) {
      name
    }
  }
`

const SET_ALBUM_VOLUME = gql`
  mutation updateAlbumVolume($id: Int!, $volume: String!) {
    updateAlbumVolume(album_id: $id, volume: $volume) {
      name
    }
  }
`

const SET_TRACK_NAME = gql`
  mutation updateTrackName($id: Int, $name: String) {
    updateTrackName(id: $id, name: $name) {
      name
    }
  }
`

const SET_TRACK_ARTIST = gql`
  mutation updateTrackArtist($id: Int, $artist: String) {
    updateTrackArtist(id: $id, artist: $artist) {
      name
    }
  }
`

beforeAll(async () => {
  const { server, db } = await constructTestServer()

  SERVER = server
  DB = db
})

afterAll(() => {
  stopTestServer(SERVER)
})

describe('Queries', () => {
  it('uses resolver', async () => {
    const [{ id }] = await resolvers.Query.tracks({}, {limit: 1}, {dataSources: {db: DB}})
    expect(typeof id).toEqual('number')

    const track = await DB.track(id)
    expect(typeof track).toEqual('object')
    expect(track.id).toEqual(id)
  })

  it('runs query', async () => {
    const { query } = createTestClient(SERVER)

    {
      const { data, errors } = await query({query: TRACKS})

      expect(errors).toBeFalsy()
      expect(typeof data).toEqual('object')
      expect(data.tracks).toHaveLength(1)
      var id = data.tracks[0].id
    }

    {
      const { data, errors } = await query({query: TRACK, variables: {id}})

      expect(errors).toBeFalsy()
      expect(typeof data).toEqual('object')
      expect(data.tracks).toHaveLength(1)
      expect(data.tracks[0].id).toEqual(id)
      expect(data).toMatchSnapshot()
    }
  })

  it('gets album', async () => {
    const { query } = createTestClient(SERVER)

    const { data, errors } = await query({query: ALBUM, variables: {id: 1}})
    expect(errors).toBeFalsy()

    expect(data.albums.albums[0].album_id).toEqual(1)
    expect(Object.keys(data.albums.albums[0])).toHaveLength(4)
  })

  it('gets format', async () => {
    const { query } = createTestClient(SERVER)

    const { data, errors } = await query({query: ALBUM_WITH_FORMAT})
    expect(errors).toBeFalsy()

    expect(data.albums.albums[0].album_id).toEqual(1)
    expect(data.albums.albums[0].format).toEqual('mp3')
  })

  it('gets volume', async () => {
    const { query } = createTestClient(SERVER)

    const { data: { volumes }, errors } = await query({query: VOLUME, variables: { id: '001' }})
    expect(errors).toBeFalsy()

    expect(volumes.volumes[0].albums).toHaveLength(1)
    expect(volumes.volumes[0].albums[0].album_id).toEqual(1)

    expect(volumes.volumes[0].thumbnail).toEqual('test_artist-test_album.jpg')
  })
})

describe('Mutations', () => {
  it('adds and removes track', async () => {
    const { query, mutate } = createTestClient(SERVER)

    const input = {
      name: 'Track 1',
      artist: 'Artist 1',
      album: 'Album 1',
      volume: '001',
      albumdate: '2000'
    }

    {
      const { errors, data }  = await mutate({
        mutation: ADD_TRACK,
        variables: {input}
      })
      expect(errors).toBeFalsy()
      expect(data.addTrack.id).toBeGreaterThan(0)

      var id = data.addTrack.id
    }

    {
      const { data, errors } = await query({query: TRACK, variables: {id}})

      expect(errors).toBeFalsy()
      expect(typeof data).toEqual('object')
      expect(data.tracks[0].name).toEqual(input.name)
      expect(data.tracks[0].albumObj.album_id).toBeGreaterThan(0)
    }

    {
      const { errors, data } = await mutate({
        mutation: DELETE_TRACK,
        variables: { id }
      })
      expect(errors).toBeFalsy()
      expect(data.deleteTrack.id).toEqual(id)
    }

    {
      const { data, errors } = await query({query: TRACK, variables: { id }})

      expect(errors).toBeFalsy()
      expect(typeof data).toEqual('object')
      expect(data.tracks).toHaveLength(0)
    }
  })

  it('adds and removes album', async () => {
    const { query, mutate } = createTestClient(SERVER)

    const album = {
      name: 'Album 1',
      artist: 'Artist 1',
      volume: '001'
    }

    {
      const { errors, data }  = await mutate({
        mutation: ADD_ALBUM,
        variables: {album}
      })
      expect(errors).toBeFalsy()
      expect(+data.addAlbum).toBeGreaterThan(0)

      var id = +data.addAlbum
    }

    {
      const { data, errors } = await query({query: ALBUM, variables: { id }})

      expect(errors).toBeFalsy()
      expect(typeof data.albums).toEqual('object')
      expect(data.albums.albums).toHaveLength(1)
      expect(data.albums.albums[0].name).toEqual(album.name)
      expect(data.albums.albums[0].album_id).toEqual(id)
    }

    {
      const { errors, data } = await mutate({
        mutation: DELETE_ALBUM,
        variables: { id }
      })

      expect(errors).toBeFalsy()
      expect(data.deleteAlbumAlbum.album_id).toEqual(id)
    }

    {
      const { data, errors } = await query({query: ALBUM, variables: { id }})

      expect(errors).toBeFalsy()
      expect(typeof data.albums).toEqual('object')
      expect(data.albums.albums).toHaveLength(0)
    }
  })

  it('updates album name', async () => {
    const { mutate } = createTestClient(SERVER)

    const variables = {
      id: 1,
      name: 'New name'
    }

    const { errors, data }  = await mutate({ mutation: SET_ALBUM_NAME, variables })
    expect(errors).toBeFalsy()
    expect(typeof data).toEqual('object')
  })

  it('updates album artist', async () => {
    const { mutate } = createTestClient(SERVER)

    const variables = {
      id: 1,
      artist: 'New artist'
    }

    const { errors, data }  = await mutate({ mutation: SET_ALBUM_ARTIST, variables })
    expect(errors).toBeFalsy()
    expect(typeof data).toEqual('object')
  })

  it('updates album year', async () => {
    const { mutate } = createTestClient(SERVER)

    const variables = {
      id: 1,
      year: '2022'
    }

    const { errors, data }  = await mutate({ mutation: SET_ALBUM_YEAR, variables })
    expect(errors).toBeFalsy()
    expect(typeof data).toEqual('object')
  })

  it('updates album volume', async () => {
    const { mutate } = createTestClient(SERVER)

    const variables = {
      id: 1,
      volume: 'volume-2'
    }

    const { errors, data }  = await mutate({ mutation: SET_ALBUM_VOLUME, variables })
    expect(errors).toBeFalsy()
    expect(typeof data).toEqual('object')
  })

  it('updates track name', async () => {
    const { mutate } = createTestClient(SERVER)

    const variables = {
      id: 1,
      name: 'New name'
    }

    const { errors, data }  = await mutate({ mutation: SET_TRACK_NAME, variables })
    expect(errors).toBeFalsy()
    expect(typeof data).toEqual('object')
  })

  it('updates track artist', async () => {
    const { mutate } = createTestClient(SERVER)

    const variables = {
      id: 1,
      artist: 'New artist'
    }

    const { errors, data }  = await mutate({ mutation: SET_TRACK_ARTIST, variables })
    expect(errors).toBeFalsy()
    expect(typeof data).toEqual('object')
  })

  it('fixes missing album', async () => {
    const { mutate } = createTestClient(SERVER)

    {
      const { errors } = await mutate({ mutation: FIX_ALBUM_ALBUM, variables: { id: 1 }})
      expect(errors).toBeTruthy()
    }

    {
      const input = { volume: 'volume-1', album: 'album-1', albumdate: new Date(), filedir: 'A - B' }
      const album = { artist: 'artist-1' }
      const added = await DB.add_track (input, album)

      const { errors } = await mutate({ mutation: FIX_ALBUM_ALBUM, variables: { id: added.id }})
      expect(errors).toBeTruthy()
    }
  })
})

describe('Admin', () => {
  it('get orphans', async () => {
    const { query } = createTestClient(SERVER)

    const { data, errors } = await query({query: ALBUM_ORPHANS})
    expect(errors).toBeFalsy()

    expect(data.albumOrphans).toHaveLength(1)
  })

  it('get tracks without album', async () => {
    const { query } = createTestClient(SERVER)

    const { data, errors } = await query({query: TRACKS_WITHOUT_ALBUM})
    expect(errors).toBeFalsy()

    expect(data.tracksWithoutAlbum).toHaveLength(0)
  })

  it('get stats', async () => {
    const { query } = createTestClient(SERVER)

    const { data, errors } = await query({query: STATS})
    expect(errors).toBeFalsy()

    expect(data.stats.genres).toHaveLength(1)
    expect(data.stats.genres[0].name).toEqual("Genre 1")
  })
})

