const fs = require('fs')
const { firstValueFrom } = require('rxjs')
const { constructTestServer, stopTestServer } = require('./__utils')
const resolvers = require('../resolvers')
const scan = require('../scan')

let SERVER
let DB

beforeAll(async () => {
  const { server, db } = await constructTestServer()

  SERVER = server
  DB = db
})

afterAll(() => {
  stopTestServer(SERVER)
})

describe('Scan', () => {
  it('completes scan', async () => {

    const result = await resolvers.Query.scan(null, {id: 'volume-1'})
    expect(result.albums).toHaveLength(1)
    expect(result.albums[0].album_id).toEqual(0)

    const album = await new Promise(resolve => {
      scan.album_update.subscribe(async album => {
        await waitFor(() => album.name === 'Album 1')
        resolve(album)
      })
    })

    const thumbnail = '/tmp/artist_1-album_1.jpg'
    await waitFor(async () => {
      try {
        await fs.promises.access(thumbnail)
        return true
      } catch (e) {}
    })
  })

  it('imports', async () => {
    const thumbnail = './__tests__/fixtures/artist_1-album_1.jpg'
    await scan.import(DB, 'volume-1')
    await fs.promises.unlink(thumbnail)
  })

  it('reads cue file', async () => {
    resolvers.Query.scan(null, {id: 'cue'})

    const album = await new Promise(resolve => {
      scan.album_update.subscribe(async album => {
        await waitFor(() => album.name === 'Album 2')
        resolve(album)
      })
    })

    expect(album.cue[0].tracks[0].name).toEqual('Track 1')
    expect(album.cue[0].tracks[0].artist).toEqual('Performer 1')

    expect(album.year).toEqual(2021)
    expect(album.tracks).toHaveLength(3)
    expect(album.tracks[0].name).toEqual('Track 1')
    expect(album.tracks[0].songnb).toEqual(1)
  })

  it('reads 2 cue files', async () => {
    resolvers.Query.scan(null, {id: 'cue-x2'})

    const album = await new Promise(resolve => {
      scan.album_update.subscribe(async album => {
        await waitFor(() => album.name === 'Album 3')
        resolve(album)
      })
    })

    expect(album.cue[0].tracks[0].name).toEqual('Track 101')
    expect(album.cue[0].tracks[0].artist).toEqual('Performer 1')
    expect(album.cue[1].tracks[0].name).toEqual('Track 201')
    expect(album.cue[1].tracks[0].artist).toEqual('Performer 1')

    expect(album.tracks).toHaveLength(6)
    expect(album.tracks[0].name).toEqual('Track 101')
    expect(album.tracks[0].songnb).toEqual(101)
    expect(album.tracks[3].name).toEqual('Track 201')
    expect(album.tracks[3].songnb).toEqual(201)
  })

  it('can be run more than once', async () => {
    const albums1 = await scan.albums('volume-1')
    const albums2 = await scan.albums('volume-1')
    expect(albums1).toEqual(albums2)
  })

  it('watches for file system changes', async () => {
    const filepath = './__tests__/fixtures/volume-1/album-1/tmp.jpg'
    const [album] = await scan.albums('volume-1')
    await waitFor(() => album.images?.includes('thumbnail.jpg'))

    fs.closeSync(fs.openSync(filepath, 'w'))
    const event = await firstValueFrom(scan.album_update)
    await fs.promises.unlink(filepath)

    expect(album.images).toContain('tmp.jpg')
  })
})

function waitFor (condition) {
  return new Promise(resolve => {
    let interval = setInterval(async () => {
      const result = await condition()
      if (result) {
        interval = clearInterval(interval)
        resolve()
      }
    }, 200)
  })
}
