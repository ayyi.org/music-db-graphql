const Album = require('../album')

const data = {
  track: {
    name: 'Track',
    artist: 'Artist',
    albumdate: '2021'
  }
}

describe('Album', () => {
  it('creates album', () => {
    const album = new Album('Album')

    expect(album.name).toEqual('Album')
    expect(album.tracks).toHaveLength(0)
  })

  it('adds track', () => {
    const album = new Album()

    album.add_track(data.track)

    expect(album.tracks).toHaveLength(1)
  })

  it('has artist', () => {
    const album = new Album()

    album.add_track(data.track)

    expect(album.artist()).toEqual(data.track.artist)
  })

  it('has year', () => {
    const album = new Album()
    album.add_track(data.track)

    expect(album.year()).toEqual('2021')
  })

  it('returns thumbnail', () => {
    const album = new Album('Album')

    album.add_track(data.track)

    const thumb = album.render().thumb.split('/').pop()

    expect(thumb).toEqual('artist-album.jpg')
  })
})
