const fs = require('fs')
const { ApolloServer } = require('apollo-server')
const { HttpLink } = require('apollo-link-http')
const fetch = require('node-fetch')
const { execute } = require('apollo-link')
const Database = require('../db')
const typeDefs = fs.readFileSync('./schema.graphql', 'utf8')
const resolvers = require('../resolvers')
const Scan = require('../scan')

const defaultContext = {}

const config = {
  db: {
    type: 'sqlite3',
    filename: './__tests__/db.sqlite'
  }
}

let DB

const _constructTestServer = async ({ context = defaultContext } = {}) => {
  if (config.db.type == 'sqlite3') {
    if (fs.existsSync(config.db.filename))
      fs.unlinkSync(config.db.filename)
  }

  const db = DB = await new Database(require('knex')({
    client: config.db.type,
    connection: config.db,
    useNullAsDefault: true,
    migrations: {
      tableName: 'mp3main'
    }
  }))

  db.type = config.db.type

  if (config.db.type == 'sqlite3') {
    await db.knex.schema.createTable('mp3main', table => {
      table.increments()
      table.string('name')
      table.string('songname')
      table.string('artist')
      table.string('album')
      table.date('albumdate')
      table.int('songnb')
      table.int('min')
      table.int('sec')
      table.string('filename')
      table.string('filedir')
      table.int('filesize')
      table.string('volume')
      table.string('genre')
      table.int('album_id')
    })

    await db.knex.schema.createTable('album', table => {
      table.increments('album_id')
      table.string('name')
      table.string('info')
      table.string('artist')
    })

    await db.knex('mp3main').insert({
      artist: 'Test Artist',
      songname: 'Test Track',
      album: 'Test Album',
      songnb: 1,
      min: 3,
      sec: 30,
      album_id: 1,
      volume: '001',
      min: 3,
      sec: 30,
      filename: 'track_1.mp3',
      genre: 'Genre 1',
    })

    await db.knex('mp3main').insert({
      artist: 'Test Artist',
      songname: 'Test Track',
      album: 'Test Album',
      songnb: 1,
      album_id: 1,
      volume: '002',
      filename: 'track_2.mp3',
    })

    await db.knex('album').insert({
      name: 'Test Album',
      artist: 'Test Artist',
    })
  }

  const server = await new ApolloServer({
    typeDefs,
    resolvers,
    dataSources: () => ({ db }),
    context,
    subscriptions: {
      path: '/subscriptions',
      onConnect: (_connectionParams, _webSocket, _context) => {
      },
      onDisconnect: (_webSocket, _context) => {
      },
    }
  })

  return { server, db }
}

let ConstructPromise
const constructTestServer = async (args) => {
  if (!ConstructPromise) {
    ConstructPromise = _constructTestServer(args)
  }
  return await ConstructPromise
}

module.exports.constructTestServer = constructTestServer

const startTestServer = async server => {
  const httpServer = await server.listen({ port: 8003 })

  const link = new HttpLink({
    uri: `http://localhost:${httpServer.port}`,
    fetch
  })

  const executeOperation = ({ query, variables = {} }) => execute(link, { query, variables })

  return {
    link,
    stop: () => httpServer.server.close(),
    graphql: executeOperation
  }
}

module.exports.startTestServer = startTestServer

const stopTestServer = server => {
  server && server.stop()
  DB && DB.knex.destroy()
  Scan.stop()
}

module.exports.stopTestServer = stopTestServer
