const { createTestClient } = require('apollo-server-testing')
const gql = require('graphql-tag')
const { constructTestServer, stopTestServer } = require('./__utils')

let SERVER

const VOLUMES = gql`
  query ($first: String) {
    volumes(first: $first) {
      volumes {
        id
        thumbnail
      }
    }
  }
`

const LATEST_VOLUMES = gql`
  query  {
    latestVolumes {
      id
      thumbnail
    }
  }
`

beforeAll(async () => {
  const { server } = await constructTestServer()

  SERVER = server
})

afterAll(() => {
  stopTestServer(SERVER)
})

describe('Volume', () => {
  let query

  it('gets volumes', async () => {
    const client = createTestClient(SERVER)
    query = client.query

    const { data, errors } = await query({query: VOLUMES})
    expect(errors).toBeFalsy()

    expect(typeof data).toEqual('object')
    expect(data.volumes.volumes).toHaveLength(2)
    const volume = data.volumes.volumes[0]
    expect(volume.id).toEqual('001')
  })

  it('gets latest volumes', async () => {
    {
      const { data, errors } = await query({query: LATEST_VOLUMES})
      expect(errors).toBeFalsy()

      expect(typeof data).toEqual('object')
      expect(data.latestVolumes).toHaveLength(1)
      const volume = data.latestVolumes[0]
      expect(volume.id).toEqual('002')
    }
  })
})
