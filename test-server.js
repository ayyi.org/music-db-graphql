const express = require('express')
const app = express()
const port = 8004

const { constructTestServer, startTestServer } = require('./__tests__/__utils')

constructTestServer().then(async ({ server }) => {
  await startTestServer(server)
  console.log(`GraphQL server listening on port 8003`)
})

app.get('/*', (req, res) => {
  res.sendFile(__dirname + '/__tests__/fixtures/thumbnail.jpg')
})

app.listen(port, () => {
  console.log(`Artwork server listening at http://localhost:${port}`)
})
