import { GraphQLResolveInfo, GraphQLScalarType, GraphQLScalarTypeConfig } from 'graphql';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
export type MakeEmpty<T extends { [key: string]: unknown }, K extends keyof T> = { [_ in K]?: never };
export type Incremental<T> = T | { [P in keyof T]?: P extends ' $fragmentName' | '__typename' ? T[P] : never };
export type RequireFields<T, K extends keyof T> = Omit<T, K> & { [P in K]-?: NonNullable<T[P]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: { input: string; output: string; }
  String: { input: string; output: string; }
  Boolean: { input: boolean; output: boolean; }
  Int: { input: number; output: number; }
  Float: { input: number; output: number; }
  Thumbnail: { input: any; output: any; }
};

export type Album = {
  __typename?: 'Album';
  album_id: Scalars['Int']['output'];
  artist?: Maybe<Scalars['String']['output']>;
  format?: Maybe<Scalars['String']['output']>;
  name?: Maybe<Scalars['String']['output']>;
  thumbnail?: Maybe<Scalars['String']['output']>;
  tracks: Array<Track>;
};

export type AlbumInput = {
  artist?: InputMaybe<Scalars['String']['input']>;
  name: Scalars['String']['input'];
  volume?: InputMaybe<Scalars['String']['input']>;
};

export type AlbumRemoved = {
  __typename?: 'AlbumRemoved';
  album_id: Scalars['Int']['output'];
};

export type AlbumScan = {
  __typename?: 'AlbumScan';
  album_id: Scalars['Int']['output'];
  artist?: Maybe<Scalars['String']['output']>;
  name: Scalars['String']['output'];
  thumbnail?: Maybe<Scalars['String']['output']>;
  tracks?: Maybe<Array<TrackScan>>;
  volume?: Maybe<Scalars['String']['output']>;
  year?: Maybe<Scalars['String']['output']>;
};

export type AlbumsConnection = {
  __typename?: 'AlbumsConnection';
  albums?: Maybe<Array<Album>>;
  pager?: Maybe<PageInfo>;
};

export enum CacheControlScope {
  Private = 'PRIVATE',
  Public = 'PUBLIC'
}

export type FileRemoved = {
  __typename?: 'FileRemoved';
  album_id?: Maybe<Scalars['Int']['output']>;
  id?: Maybe<Scalars['Int']['output']>;
};

export type Genre = {
  __typename?: 'Genre';
  count: Scalars['Int']['output'];
  name: Scalars['String']['output'];
};

export type Mutation = {
  __typename?: 'Mutation';
  addAlbum?: Maybe<Scalars['ID']['output']>;
  addTrack?: Maybe<Track>;
  deleteAlbumAlbum?: Maybe<Album>;
  deleteTrack?: Maybe<Track>;
  fixAlbumAlbum?: Maybe<Array<Maybe<Track>>>;
  import?: Maybe<Volume>;
  updateAlbumArtist?: Maybe<Album>;
  updateAlbumName?: Maybe<Album>;
  updateAlbumVolume?: Maybe<Album>;
  updateAlbumYear?: Maybe<Album>;
  updateTrackArtist?: Maybe<Track>;
  updateTrackName?: Maybe<Track>;
};


export type MutationAddAlbumArgs = {
  album?: InputMaybe<AlbumInput>;
};


export type MutationAddTrackArgs = {
  input?: InputMaybe<TrackInput>;
};


export type MutationDeleteAlbumAlbumArgs = {
  id?: InputMaybe<Scalars['Int']['input']>;
};


export type MutationDeleteTrackArgs = {
  id?: InputMaybe<Scalars['Int']['input']>;
};


export type MutationFixAlbumAlbumArgs = {
  id?: InputMaybe<Scalars['Int']['input']>;
};


export type MutationImportArgs = {
  id?: InputMaybe<Scalars['String']['input']>;
};


export type MutationUpdateAlbumArtistArgs = {
  album_id: Scalars['Int']['input'];
  artist: Scalars['String']['input'];
};


export type MutationUpdateAlbumNameArgs = {
  album_id: Scalars['Int']['input'];
  name: Scalars['String']['input'];
};


export type MutationUpdateAlbumVolumeArgs = {
  album_id: Scalars['Int']['input'];
  volume: Scalars['String']['input'];
};


export type MutationUpdateAlbumYearArgs = {
  album_id: Scalars['Int']['input'];
  year: Scalars['String']['input'];
};


export type MutationUpdateTrackArtistArgs = {
  artist?: InputMaybe<Scalars['String']['input']>;
  id?: InputMaybe<Scalars['Int']['input']>;
};


export type MutationUpdateTrackNameArgs = {
  id?: InputMaybe<Scalars['Int']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
};

export type PageInfo = {
  __typename?: 'PageInfo';
  count?: Maybe<Scalars['Int']['output']>;
};

export type Query = {
  __typename?: 'Query';
  albumOrphans?: Maybe<Array<Maybe<Album>>>;
  albums?: Maybe<AlbumsConnection>;
  latestVolumes?: Maybe<Array<Maybe<Volume>>>;
  scan?: Maybe<Scan>;
  stats?: Maybe<Stats>;
  tracks?: Maybe<Array<Maybe<Track>>>;
  tracksWithoutAlbum?: Maybe<Array<Maybe<Track>>>;
  volumes?: Maybe<VolumesConnection>;
};


export type QueryAlbumsArgs = {
  id?: InputMaybe<Scalars['Int']['input']>;
  limit?: InputMaybe<Scalars['Int']['input']>;
};


export type QueryScanArgs = {
  id?: InputMaybe<Scalars['String']['input']>;
};


export type QueryTracksArgs = {
  id?: InputMaybe<Scalars['Int']['input']>;
  limit?: InputMaybe<Scalars['Int']['input']>;
  search?: InputMaybe<Scalars['String']['input']>;
};


export type QueryTracksWithoutAlbumArgs = {
  volume?: InputMaybe<Scalars['String']['input']>;
};


export type QueryVolumesArgs = {
  first?: InputMaybe<Scalars['String']['input']>;
  id?: InputMaybe<Scalars['ID']['input']>;
};

export type Scan = {
  __typename?: 'Scan';
  albums?: Maybe<Array<Maybe<AlbumScan>>>;
  inProgress?: Maybe<Scalars['Boolean']['output']>;
};

export type Stats = {
  __typename?: 'Stats';
  genres?: Maybe<Array<Maybe<Genre>>>;
};

export type Subscription = {
  __typename?: 'Subscription';
  albumRemoved?: Maybe<AlbumRemoved>;
  albumUpdated?: Maybe<AlbumScan>;
  fileAdded?: Maybe<TrackScan>;
  fileRemoved?: Maybe<FileRemoved>;
  volumeUpdated?: Maybe<Scalars['String']['output']>;
};

export type Track = {
  __typename?: 'Track';
  album?: Maybe<Scalars['String']['output']>;
  albumObj?: Maybe<Album>;
  album_name?: Maybe<Scalars['String']['output']>;
  artist?: Maybe<Scalars['String']['output']>;
  filename?: Maybe<Scalars['String']['output']>;
  id: Scalars['Int']['output'];
  min?: Maybe<Scalars['Int']['output']>;
  name?: Maybe<Scalars['String']['output']>;
  sec?: Maybe<Scalars['Int']['output']>;
  songnb?: Maybe<Scalars['Int']['output']>;
  volume?: Maybe<Scalars['String']['output']>;
  year?: Maybe<Scalars['String']['output']>;
};


export type TrackAlbumObjArgs = {
  id?: InputMaybe<Scalars['Int']['input']>;
};

export type TrackInput = {
  album?: InputMaybe<Scalars['String']['input']>;
  album_id?: InputMaybe<Scalars['Int']['input']>;
  albumdate?: InputMaybe<Scalars['String']['input']>;
  artist?: InputMaybe<Scalars['String']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
  songnb?: InputMaybe<Scalars['Int']['input']>;
  volume?: InputMaybe<Scalars['String']['input']>;
};

export type TrackScan = {
  __typename?: 'TrackScan';
  album?: Maybe<Scalars['String']['output']>;
  album_id?: Maybe<Scalars['Int']['output']>;
  artist?: Maybe<Scalars['String']['output']>;
  filename?: Maybe<Scalars['String']['output']>;
  id?: Maybe<Scalars['Int']['output']>;
  name?: Maybe<Scalars['String']['output']>;
  songnb?: Maybe<Scalars['Int']['output']>;
  time?: Maybe<Scalars['String']['output']>;
  volume?: Maybe<Scalars['String']['output']>;
};

export type Volume = {
  __typename?: 'Volume';
  albums?: Maybe<Array<Album>>;
  id: Scalars['ID']['output'];
  thumbnail?: Maybe<Scalars['Thumbnail']['output']>;
};

export type VolumesConnection = {
  __typename?: 'VolumesConnection';
  pager?: Maybe<PageInfo>;
  volumes?: Maybe<Array<Volume>>;
};

export type WithIndex<TObject> = TObject & Record<string, any>;
export type ResolversObject<TObject> = WithIndex<TObject>;

export type ResolverTypeWrapper<T> = Promise<T> | T;


export type ResolverWithResolve<TResult, TParent, TContext, TArgs> = {
  resolve: ResolverFn<TResult, TParent, TContext, TArgs>;
};
export type Resolver<TResult, TParent = {}, TContext = {}, TArgs = {}> = ResolverFn<TResult, TParent, TContext, TArgs> | ResolverWithResolve<TResult, TParent, TContext, TArgs>;

export type ResolverFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => Promise<TResult> | TResult;

export type SubscriptionSubscribeFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => AsyncIterable<TResult> | Promise<AsyncIterable<TResult>>;

export type SubscriptionResolveFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => TResult | Promise<TResult>;

export interface SubscriptionSubscriberObject<TResult, TKey extends string, TParent, TContext, TArgs> {
  subscribe: SubscriptionSubscribeFn<{ [key in TKey]: TResult }, TParent, TContext, TArgs>;
  resolve?: SubscriptionResolveFn<TResult, { [key in TKey]: TResult }, TContext, TArgs>;
}

export interface SubscriptionResolverObject<TResult, TParent, TContext, TArgs> {
  subscribe: SubscriptionSubscribeFn<any, TParent, TContext, TArgs>;
  resolve: SubscriptionResolveFn<TResult, any, TContext, TArgs>;
}

export type SubscriptionObject<TResult, TKey extends string, TParent, TContext, TArgs> =
  | SubscriptionSubscriberObject<TResult, TKey, TParent, TContext, TArgs>
  | SubscriptionResolverObject<TResult, TParent, TContext, TArgs>;

export type SubscriptionResolver<TResult, TKey extends string, TParent = {}, TContext = {}, TArgs = {}> =
  | ((...args: any[]) => SubscriptionObject<TResult, TKey, TParent, TContext, TArgs>)
  | SubscriptionObject<TResult, TKey, TParent, TContext, TArgs>;

export type TypeResolveFn<TTypes, TParent = {}, TContext = {}> = (
  parent: TParent,
  context: TContext,
  info: GraphQLResolveInfo
) => Maybe<TTypes> | Promise<Maybe<TTypes>>;

export type IsTypeOfResolverFn<T = {}, TContext = {}> = (obj: T, context: TContext, info: GraphQLResolveInfo) => boolean | Promise<boolean>;

export type NextResolverFn<T> = () => Promise<T>;

export type DirectiveResolverFn<TResult = {}, TParent = {}, TContext = {}, TArgs = {}> = (
  next: NextResolverFn<TResult>,
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => TResult | Promise<TResult>;



/** Mapping between all available schema types and the resolvers types */
export type ResolversTypes = ResolversObject<{
  Album: ResolverTypeWrapper<Album>;
  AlbumInput: AlbumInput;
  AlbumRemoved: ResolverTypeWrapper<AlbumRemoved>;
  AlbumScan: ResolverTypeWrapper<AlbumScan>;
  AlbumsConnection: ResolverTypeWrapper<AlbumsConnection>;
  Boolean: ResolverTypeWrapper<Scalars['Boolean']['output']>;
  CacheControlScope: CacheControlScope;
  FileRemoved: ResolverTypeWrapper<FileRemoved>;
  Genre: ResolverTypeWrapper<Genre>;
  ID: ResolverTypeWrapper<Scalars['ID']['output']>;
  Int: ResolverTypeWrapper<Scalars['Int']['output']>;
  Mutation: ResolverTypeWrapper<{}>;
  PageInfo: ResolverTypeWrapper<PageInfo>;
  Query: ResolverTypeWrapper<{}>;
  Scan: ResolverTypeWrapper<Scan>;
  Stats: ResolverTypeWrapper<Stats>;
  String: ResolverTypeWrapper<Scalars['String']['output']>;
  Subscription: ResolverTypeWrapper<{}>;
  Thumbnail: ResolverTypeWrapper<Scalars['Thumbnail']['output']>;
  Track: ResolverTypeWrapper<Track>;
  TrackInput: TrackInput;
  TrackScan: ResolverTypeWrapper<TrackScan>;
  Volume: ResolverTypeWrapper<Volume>;
  VolumesConnection: ResolverTypeWrapper<VolumesConnection>;
}>;

/** Mapping between all available schema types and the resolvers parents */
export type ResolversParentTypes = ResolversObject<{
  Album: Album;
  AlbumInput: AlbumInput;
  AlbumRemoved: AlbumRemoved;
  AlbumScan: AlbumScan;
  AlbumsConnection: AlbumsConnection;
  Boolean: Scalars['Boolean']['output'];
  FileRemoved: FileRemoved;
  Genre: Genre;
  ID: Scalars['ID']['output'];
  Int: Scalars['Int']['output'];
  Mutation: {};
  PageInfo: PageInfo;
  Query: {};
  Scan: Scan;
  Stats: Stats;
  String: Scalars['String']['output'];
  Subscription: {};
  Thumbnail: Scalars['Thumbnail']['output'];
  Track: Track;
  TrackInput: TrackInput;
  TrackScan: TrackScan;
  Volume: Volume;
  VolumesConnection: VolumesConnection;
}>;

export type CacheControlDirectiveArgs = {
  maxAge?: Maybe<Scalars['Int']['input']>;
  scope?: Maybe<CacheControlScope>;
};

export type CacheControlDirectiveResolver<Result, Parent, ContextType = any, Args = CacheControlDirectiveArgs> = DirectiveResolverFn<Result, Parent, ContextType, Args>;

export type LogCacheDirectiveArgs = {
  identifier?: Maybe<Scalars['String']['input']>;
  type: Scalars['String']['input'];
};

export type LogCacheDirectiveResolver<Result, Parent, ContextType = any, Args = LogCacheDirectiveArgs> = DirectiveResolverFn<Result, Parent, ContextType, Args>;

export type PurgeCacheDirectiveArgs = {
  identifier?: Maybe<Scalars['String']['input']>;
  type: Scalars['String']['input'];
};

export type PurgeCacheDirectiveResolver<Result, Parent, ContextType = any, Args = PurgeCacheDirectiveArgs> = DirectiveResolverFn<Result, Parent, ContextType, Args>;

export type AlbumResolvers<ContextType = any, ParentType extends ResolversParentTypes['Album'] = ResolversParentTypes['Album']> = ResolversObject<{
  album_id?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  artist?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  format?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  name?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  thumbnail?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  tracks?: Resolver<Array<ResolversTypes['Track']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type AlbumRemovedResolvers<ContextType = any, ParentType extends ResolversParentTypes['AlbumRemoved'] = ResolversParentTypes['AlbumRemoved']> = ResolversObject<{
  album_id?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type AlbumScanResolvers<ContextType = any, ParentType extends ResolversParentTypes['AlbumScan'] = ResolversParentTypes['AlbumScan']> = ResolversObject<{
  album_id?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  artist?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  name?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  thumbnail?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  tracks?: Resolver<Maybe<Array<ResolversTypes['TrackScan']>>, ParentType, ContextType>;
  volume?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  year?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type AlbumsConnectionResolvers<ContextType = any, ParentType extends ResolversParentTypes['AlbumsConnection'] = ResolversParentTypes['AlbumsConnection']> = ResolversObject<{
  albums?: Resolver<Maybe<Array<ResolversTypes['Album']>>, ParentType, ContextType>;
  pager?: Resolver<Maybe<ResolversTypes['PageInfo']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type FileRemovedResolvers<ContextType = any, ParentType extends ResolversParentTypes['FileRemoved'] = ResolversParentTypes['FileRemoved']> = ResolversObject<{
  album_id?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>;
  id?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type GenreResolvers<ContextType = any, ParentType extends ResolversParentTypes['Genre'] = ResolversParentTypes['Genre']> = ResolversObject<{
  count?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  name?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type MutationResolvers<ContextType = any, ParentType extends ResolversParentTypes['Mutation'] = ResolversParentTypes['Mutation']> = ResolversObject<{
  addAlbum?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType, Partial<MutationAddAlbumArgs>>;
  addTrack?: Resolver<Maybe<ResolversTypes['Track']>, ParentType, ContextType, Partial<MutationAddTrackArgs>>;
  deleteAlbumAlbum?: Resolver<Maybe<ResolversTypes['Album']>, ParentType, ContextType, Partial<MutationDeleteAlbumAlbumArgs>>;
  deleteTrack?: Resolver<Maybe<ResolversTypes['Track']>, ParentType, ContextType, Partial<MutationDeleteTrackArgs>>;
  fixAlbumAlbum?: Resolver<Maybe<Array<Maybe<ResolversTypes['Track']>>>, ParentType, ContextType, Partial<MutationFixAlbumAlbumArgs>>;
  import?: Resolver<Maybe<ResolversTypes['Volume']>, ParentType, ContextType, Partial<MutationImportArgs>>;
  updateAlbumArtist?: Resolver<Maybe<ResolversTypes['Album']>, ParentType, ContextType, RequireFields<MutationUpdateAlbumArtistArgs, 'album_id' | 'artist'>>;
  updateAlbumName?: Resolver<Maybe<ResolversTypes['Album']>, ParentType, ContextType, RequireFields<MutationUpdateAlbumNameArgs, 'album_id' | 'name'>>;
  updateAlbumVolume?: Resolver<Maybe<ResolversTypes['Album']>, ParentType, ContextType, RequireFields<MutationUpdateAlbumVolumeArgs, 'album_id' | 'volume'>>;
  updateAlbumYear?: Resolver<Maybe<ResolversTypes['Album']>, ParentType, ContextType, RequireFields<MutationUpdateAlbumYearArgs, 'album_id' | 'year'>>;
  updateTrackArtist?: Resolver<Maybe<ResolversTypes['Track']>, ParentType, ContextType, Partial<MutationUpdateTrackArtistArgs>>;
  updateTrackName?: Resolver<Maybe<ResolversTypes['Track']>, ParentType, ContextType, Partial<MutationUpdateTrackNameArgs>>;
}>;

export type PageInfoResolvers<ContextType = any, ParentType extends ResolversParentTypes['PageInfo'] = ResolversParentTypes['PageInfo']> = ResolversObject<{
  count?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type QueryResolvers<ContextType = any, ParentType extends ResolversParentTypes['Query'] = ResolversParentTypes['Query']> = ResolversObject<{
  albumOrphans?: Resolver<Maybe<Array<Maybe<ResolversTypes['Album']>>>, ParentType, ContextType>;
  albums?: Resolver<Maybe<ResolversTypes['AlbumsConnection']>, ParentType, ContextType, Partial<QueryAlbumsArgs>>;
  latestVolumes?: Resolver<Maybe<Array<Maybe<ResolversTypes['Volume']>>>, ParentType, ContextType>;
  scan?: Resolver<Maybe<ResolversTypes['Scan']>, ParentType, ContextType, Partial<QueryScanArgs>>;
  stats?: Resolver<Maybe<ResolversTypes['Stats']>, ParentType, ContextType>;
  tracks?: Resolver<Maybe<Array<Maybe<ResolversTypes['Track']>>>, ParentType, ContextType, Partial<QueryTracksArgs>>;
  tracksWithoutAlbum?: Resolver<Maybe<Array<Maybe<ResolversTypes['Track']>>>, ParentType, ContextType, Partial<QueryTracksWithoutAlbumArgs>>;
  volumes?: Resolver<Maybe<ResolversTypes['VolumesConnection']>, ParentType, ContextType, Partial<QueryVolumesArgs>>;
}>;

export type ScanResolvers<ContextType = any, ParentType extends ResolversParentTypes['Scan'] = ResolversParentTypes['Scan']> = ResolversObject<{
  albums?: Resolver<Maybe<Array<Maybe<ResolversTypes['AlbumScan']>>>, ParentType, ContextType>;
  inProgress?: Resolver<Maybe<ResolversTypes['Boolean']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type StatsResolvers<ContextType = any, ParentType extends ResolversParentTypes['Stats'] = ResolversParentTypes['Stats']> = ResolversObject<{
  genres?: Resolver<Maybe<Array<Maybe<ResolversTypes['Genre']>>>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type SubscriptionResolvers<ContextType = any, ParentType extends ResolversParentTypes['Subscription'] = ResolversParentTypes['Subscription']> = ResolversObject<{
  albumRemoved?: SubscriptionResolver<Maybe<ResolversTypes['AlbumRemoved']>, "albumRemoved", ParentType, ContextType>;
  albumUpdated?: SubscriptionResolver<Maybe<ResolversTypes['AlbumScan']>, "albumUpdated", ParentType, ContextType>;
  fileAdded?: SubscriptionResolver<Maybe<ResolversTypes['TrackScan']>, "fileAdded", ParentType, ContextType>;
  fileRemoved?: SubscriptionResolver<Maybe<ResolversTypes['FileRemoved']>, "fileRemoved", ParentType, ContextType>;
  volumeUpdated?: SubscriptionResolver<Maybe<ResolversTypes['String']>, "volumeUpdated", ParentType, ContextType>;
}>;

export interface ThumbnailScalarConfig extends GraphQLScalarTypeConfig<ResolversTypes['Thumbnail'], any> {
  name: 'Thumbnail';
}

export type TrackResolvers<ContextType = any, ParentType extends ResolversParentTypes['Track'] = ResolversParentTypes['Track']> = ResolversObject<{
  album?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  albumObj?: Resolver<Maybe<ResolversTypes['Album']>, ParentType, ContextType, Partial<TrackAlbumObjArgs>>;
  album_name?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  artist?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  filename?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  id?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  min?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>;
  name?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  sec?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>;
  songnb?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>;
  volume?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  year?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type TrackScanResolvers<ContextType = any, ParentType extends ResolversParentTypes['TrackScan'] = ResolversParentTypes['TrackScan']> = ResolversObject<{
  album?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  album_id?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>;
  artist?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  filename?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  id?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>;
  name?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  songnb?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>;
  time?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  volume?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type VolumeResolvers<ContextType = any, ParentType extends ResolversParentTypes['Volume'] = ResolversParentTypes['Volume']> = ResolversObject<{
  albums?: Resolver<Maybe<Array<ResolversTypes['Album']>>, ParentType, ContextType>;
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  thumbnail?: Resolver<Maybe<ResolversTypes['Thumbnail']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type VolumesConnectionResolvers<ContextType = any, ParentType extends ResolversParentTypes['VolumesConnection'] = ResolversParentTypes['VolumesConnection']> = ResolversObject<{
  pager?: Resolver<Maybe<ResolversTypes['PageInfo']>, ParentType, ContextType>;
  volumes?: Resolver<Maybe<Array<ResolversTypes['Volume']>>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type Resolvers<ContextType = any> = ResolversObject<{
  Album?: AlbumResolvers<ContextType>;
  AlbumRemoved?: AlbumRemovedResolvers<ContextType>;
  AlbumScan?: AlbumScanResolvers<ContextType>;
  AlbumsConnection?: AlbumsConnectionResolvers<ContextType>;
  FileRemoved?: FileRemovedResolvers<ContextType>;
  Genre?: GenreResolvers<ContextType>;
  Mutation?: MutationResolvers<ContextType>;
  PageInfo?: PageInfoResolvers<ContextType>;
  Query?: QueryResolvers<ContextType>;
  Scan?: ScanResolvers<ContextType>;
  Stats?: StatsResolvers<ContextType>;
  Subscription?: SubscriptionResolvers<ContextType>;
  Thumbnail?: GraphQLScalarType;
  Track?: TrackResolvers<ContextType>;
  TrackScan?: TrackScanResolvers<ContextType>;
  Volume?: VolumeResolvers<ContextType>;
  VolumesConnection?: VolumesConnectionResolvers<ContextType>;
}>;

export type DirectiveResolvers<ContextType = any> = ResolversObject<{
  cacheControl?: CacheControlDirectiveResolver<any, any, ContextType>;
  logCache?: LogCacheDirectiveResolver<any, any, ContextType>;
  purgeCache?: PurgeCacheDirectiveResolver<any, any, ContextType>;
}>;
