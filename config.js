const fs = require('fs')
const yaml = require('js-yaml')

// NODE_ENV is set automatically by Jest
const dir = process.env.NODE_ENV === 'test' ? '__tests__/' : ''

const config = yaml.load(
  fs.readFileSync(
    fs.existsSync(`./${dir}config.yaml`)
      ? `./${dir}config.yaml`
      : './__tests__/config.yaml',
    'utf8'
  )
)

module.exports = config
