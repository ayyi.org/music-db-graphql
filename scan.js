const fs = require('fs')
const util = require('util')
const mime = require('mime')
const mm = require('music-metadata')
const { Observable, Subject, from } = require('rxjs')
const { toArray, debounceTime, map, concatMap, filter } = require('rxjs/operators')
const onChange = require('on-change')
const im = require('imagemagick')
const cueparser = require('cue-parser')
const config = require('./config')
const Album = require('./album')

const audio_extns = ['mp3', 'm4a', 'mp4', 'flac', 'ogg', 'opus', 'ape']
const image_extns = ['jpg', 'jpeg', 'png', 'gif', 'bmp', 'webp']

const cache = {}
const pending = {} // TODO make private to the queues.

const ac = new AbortController();
const { signal } = ac;

const pre_queue = new Subject()
pre_queue.subscribe(val => {
  const { volume, dir } = val

  if (pending[volume][dir]) {
    clearTimeout(pending[volume][dir])
  }

  pending[volume][dir] = setTimeout(() => {
    queue.next(val)
  }, 1000) // wait for fs changes to stabilise
})

let i = 0
let t = 0
const queue = new Subject()
  .pipe(concatMap(val =>
    new Promise(async resolve => {
      const {volume, dir} = val
      const ii = i++

      cache[volume][dir].tracks?.forEach(track => Scan.track_removed.next(track));
      cache[volume][dir].tracks = []
      cache[volume][dir].artist = null

      if (!file_exists(path(config.scan_path, volume, dir))) {
        Scan.album_removed.next(cache[volume][dir]);
        delete cache[volume][dir]
        return resolve(val)
      }

      const old_album_name = cache[volume][dir].name

      await Scan._get_dir_data(volume, dir, t++)

      delete pending[volume][dir]
      ;(cache[volume][dir].name !== old_album_name) && Scan.album_update.next(cache[volume][dir])

      resolve(val)
    })
  ))
queue.subscribe(() => {})

const Scan = {
  /*
   *  Resolve with a list of albums for the given volume
   */
  albums: async volume => {
    if (!cache[volume]) cache[volume] = {}
    if (!pending[volume]) pending[volume] = {}

    if (Object.keys(cache[volume]).length)
      return Promise.resolve(Object.keys(cache[volume]).map(a => cache[volume][a]))

    return new Observable(
      async observable => {
        let album_id = 0

        scan_dir(volume)
          .then(async (dirs) => {
            for await (const i of dirs.generator()) {
              (() => {
                const dir = i.dir
                fs.watch(`${config.scan_path}/${volume}/${i.dir}`, {persistent: true, signal}, async (eventType, filename) => {
                  switch (eventType) {
                    case 'rename':
                    case 'change':
                      break;
                  }
                  if (filename.includes('.jpg')) {
                    if (file_exists(path(config.scan_path, volume, dir, filename))) {
                      const album = cache[volume][dir]
                      if (album) {
                        album.images.push(filename)
                        await make_thumbnail(volume, dir)
                        Scan.album_update.next(album)
                      }
                    }
                  } else if (filename.includes('.swp')) {
                    // ignore
                  } else {
                    pre_queue.next({ volume, dir })
                  }
                })

                new Observable(o => {
                  const album = (cache[volume][i.dir] = cache[volume][i.dir] || onChange(
                    {tracks: [], images: []},
                    () => {
                      // once the album has a name, emit it
                      album.name && o.next(album)
                    }
                  ))

                  observable.next({
                    album_id,
                    name: i.dir,
                    tracks: album.tracks
                  })

                  cache[volume][i.dir].album_id = album_id++

                  pre_queue.next({ volume, dir: i.dir })
                })
                .pipe(debounceTime(2000))
                .subscribe(album => Scan.album_update.next(album))
              })()
            }
            observable.complete()
          })
          .catch(e => observable.complete(e))
      })
      .pipe(toArray())
      .toPromise()
  },

  /*
   *  Observable that iterates over each track in each directory, gets metadata, and emits when each track is complete
   */
  track_added: new Subject(),
  track_removed: new Subject(),
  album_removed: new Subject(),
  album_update: new Subject(),

  _get_dir_data: async (volume, dir, id) => {
    const album = cache[volume][dir]
    album.cue = undefined
    let t = 0

    await read_dir(
      volume,
      dir,
      async track => {
        if (!album.tracks.find(v => v.filename == track.filename)) {
          const item = {
            ...track,
            ...await get_metadata(album, `${config.scan_path}/${volume}/${dir}/${track.filename}`),
            id: (id + 1) * 100 + (++t),
            album_id: album.album_id
          }

          album.artist = item.albumartist || (
            album.artist
              ? (item.artist === album.artist ? item.artist : 'va')
              : item.artist
          )
          if (['various artists', 'various'].includes(album.artist?.toLowerCase())) album.artist = 'va'
          album.name = item.album?.replace(/^various$/, 'va') // TODO can change - multiple albums

          album.tracks.push(item)
        }
      },
      image => {
        album.images.includes(image) || album.images.push(image)
      },
      cue => {
        album.cue ||= []
        album.cue.push({ file: cue })
      },
      nfo => {
        album.nfofile = nfo
      }
    )

    await make_thumbnail(volume, dir)

    if (album.cue)
      get_tracks_from_cue(volume, dir, album, id)
    if (album.tracks.length === 1)
      get_tracks_from_nfo(volume, dir, album, id)

    if (album.cue) {
      album.year = album.tracks[0]?.year

      album.tracks = []
      album.cue.forEach((cue, i) => {
        cue.tracks && album.tracks.push(...cue.tracks)
      })
      album.tracks.forEach(track => track.album ||= album.name)
      album.tracks.forEach(track => track.album_id = album.album_id)
    }

    album.tracks.forEach(item => Scan.track_added.next(item))
  },

  /*
   *  Add the volume to the database
   */
  import: async (db, volume) => {
    const keys = Object.keys(cache[volume])
    for (const d in keys) {
      const dir = keys[d]
      const album = cache[volume][dir]
      if (album.tracks.length) {
        let album_id
        for (let i in album.tracks) {
          const { name, artist, album: a, filename, time, songnb, year, genre, imported } = album.tracks[i]
          if (imported) continue

          const t = time.split(':')
          const file = path(config.scan_path, volume, dir, filename)

          const input = {
            name, artist, filename, songnb, genre, volume,
            min: t[0],
            sec: t[1],
            album: a,
            album_id,
            albumdate: new Date(year || album.year, 0),
            filedir: dir,
            filesize: fs.statSync(file)?.size
          }
          const res = await db.add_track(input, album)
          album_id = res.album_id
          album.tracks[i].imported = true
        }

        await import_thumbnail (album)

        await new Promise(resolve => setTimeout(resolve, 1000)) // try and get UI updates faster
      }
    }
  },

  stop: () => {
    ac.abort()
  }
}

async function make_thumbnail (volume, dir) {
  const album = cache[volume][dir]

  const image = best_image(album)
  if (image) {
    const src = image[0] === '/' ? image : path(config.scan_path, volume, dir, image)
    const dest = path('/tmp', Album.prototype._to_filename(album.artist + '-' + album.name) + '.jpg')

    await image_resize(src, dest)
    album.thumbnail = base64Image(dest)
  }
}

function import_thumbnail (album) {
  const filename = Album.prototype._to_filename(album.artist + '-' + album.name) + '.jpg'
  const target = path(config.covers, filename)

  if (album.images.length && !file_exists(target)) {
    try {
      return fs.promises.copyFile(
        '/tmp/' + filename,
        target,
        0
      )
    } catch (err) {
      console.warn('thumbnail copy failed:', err)
    }
  }
}

function path () {
  return Array.prototype.join.call(arguments, '/')
}

async function get_metadata (album, filename) {
  return mm.parseFile(filename)
    .then(async metadata => {
      //console.log(util.inspect(metadata, { showHidden: false, depth: null }))
      const common = metadata.common
      const track = {
        filename: filename.split('/').pop(),
        album: common.album,
        songnb: common.track.no || null,
        artist: common.artists?.join(', ') || common.artist,
        albumartist: common.albumartist,
        name: common.title,
        time: Math.floor((metadata.format.duration || 0)/ 60) + ':' + ('0' + Math.floor((metadata.format.duration || 0) % 60)).slice(-2),
        year: common.year,
        genre: common.genre && common.genre[0]
      }

      if (!album.year) album.year = track.year

      if (common.picture) {
        const f = path('/tmp', track.filename.replace(/.m4a|.flac/, '.jpg'))
        try {
          await fs.promises.writeFile(f, common.picture[0].data)
          album.images.push(f)
        } catch (err) {
          console.log(err)
        }
      }

      return track
    })
}

function best_image (album) {
  images = album.images && album.images.length ? album.images : album.embedded
  if (!images) return

  let best
  let score = -1
  images.forEach(image => {
    if (/front/i.test(image)) {
      if (score < 2) {
        score = 2
        best = image
      }
    }else if (/cover/i.test(image)) {
      if (score < 1) {
        score = 1
        best = image
      }
    } else if (/folder/i.test(image)) {
      if (score < 1) {
        score = 1
        best = image
      }
    } else {
      if (score < 0) {
        score = 0
        best = image
      }
    }
  })
  return best
}

function image_resize (src, dest) {
  return new Promise(done => {
    const dest_leaf = dest.split('/').pop()
    const resized = path('/tmp', dest_leaf)

    // this appears not to work for files with non-ascii characters
    // -as we are only using it temporarily, it doesnt seem worth fixing.
    im.identify(src, (err, _features) => {
      if (err) {
        return done(err)
      }

      // https://github.com/yourdeveloper/node-imagemagick
      im.crop({
        srcPath: src,
        dstPath: resized,
        width: 110,
        height: 110,
      }, (_err, _stdout, stderr) => {
        stderr && console.log('resize: stderr=', stderr)

        if (resized !== dest) {
          fs.copyFile(
            resized,
            dest,
            0,
            err => {
              if (err) console.warn('file copy failed:', err)
              done()
            }
          )
        } else {
          done()
        }
      })
    })
  })
}

const read_dir = async (volume, dir, on_track, on_image, on_cue, on_nfo) => {
  return new Promise((resolve, reject) => {
    fs.readdir(path(config.scan_path, volume, dir), async (err, files) => {
      if (err) return reject(err)

      let f = 0
      while (files?.[f]) {
        const file = files[f++]

        if (file.includes('.')) {
          const extn = file.split('.').pop()
          if (~audio_extns.indexOf(extn)) {
            await on_track({filename: file})
          } else if (~image_extns.indexOf(extn)) {
            on_image(file)
          } else if (['cue'].includes(extn)) {
            on_cue(file)
          } else if (['nfo'].includes(extn)) {
            on_nfo(file)
          }
        }
      }
      resolve()
    })
  })
}

async function scan_dir (volume) {
  return new Promise((resolve, reject) => {
    fs.promises.readdir(path(config.scan_path, volume))
      .then(dirs => {
        async function* generator() {
          let d = 0
          while (true) {
            const album = {
              dir: dirs?.[d++],
            }
            if (!album.dir) {
              break
            }
            const stat = await fs.promises.lstat(path(config.scan_path, volume, album.dir))
            if (stat.isDirectory())
              yield album
          }
        }

        resolve({generator})
      })
      .catch(reject)
  })
}

function get_tracks_from_cue(volume, dir, album, album_id) {
  if (![1,2].includes(album.tracks.length)) {
    album.cue = undefined
    return
  }

  album.cue.forEach((cue, c) => {
    try {
      const cuesheet = cueparser.parse(path(config.scan_path, volume, dir, cue.file))
      if (cuesheet.rem) console.log('cuesheet.rem=', cuesheet.rem)
      cuesheet.files[0].tracks.length || console.warn('no tracks found in cuesheet')
      album.cue[c].tracks = cuesheet.files[0].tracks.map((v, i) => {
        let dt = '0:00'
        let time0 = v.indexes[0].time
        const next = cuesheet.files[0].tracks[i+1]
        let time1 = next && next.indexes[0].time
        if (time0 && time1) {
          time0 = 60 * time0.min + time0.sec
          time1 = 60 * time1.min + time1.sec
          dt = time1 - time0
          dt = Math.floor(dt / 60) + ':' + (Math.floor(dt % 60) + '').padStart(2, '0')
        }

        if (album.cue.length > 1) {
          v.number = +v.number + 100 * (c + 1)
        }

        return {
          id: (album_id + 1) * 100 + (i),
          filename: cue.file,
          songnb: v.number,
          artist: v.performer || cuesheet.performer,
          name: v.title,
          time: dt
        }
      })
    } catch(e) {
      console.log('error processing cuefile')
      console.log(e)
    }
  })
}

function get_tracks_from_nfo(volume, dir, album, album_id) {
  if (!album.tracks.length === 1) return
  const track = album.tracks[0]

  const time = track.time?.split(':')
  if (time.length === 2 && time[0] > 30) {
    if (album.nfofile) {
      console.log('single file album: found nfo file', album.nfofile)
      const filename = path(config.scan_path, volume, dir, album.nfofile)
      const nfo = fs.readFileSync(filename).toString().split('\n')

      // discard lines that dont start with a number
      const a = nfo.filter((l) => {
        const m = l.match(/^(\d+)\s/)
        if (m && m[1] < 1000){
          return true
        }
      })

      let running_time_mode = false
      let running_time = 0

      function time_to_string(t) {
        return '' + (Math.floor(t / 60)) + ':' + (t % 60)
      }

      function string_array_to_time(a) {
        if (a.length == 2)
          return 60 * (+a[0]) + (+a[1])
        if (a.length == 3)
          return 60 * 60 * (+a[0]) + 60 * (+a[1]) + (+a[2])
      }

      const tracks = a.map((l, i) => {
        let m = l.match(/^(\d+)[ \t](.*)[ \t](\d+:\d+):*(\d*)/)
        if (m) {
          var time = m[3]
          if (i === 0 && time === '00:00')
            running_time_mode = true
          if (running_time_mode) {
            var t = time.split(':')
            if (m[4]) {
              t.push(m[4])
            }
            t = string_array_to_time(t)
            const ti = t - running_time
            running_time += ti
            time = time_to_string(ti)
          }

          let artist = track.artist
          if ((!!~['VA', 'Various'].indexOf(album.artist)) && m[2].indexOf(' - ') > 0)
          artist = m[2].split(' - ')[0]// || m[2].split(' – ')

          let split;
          if (m[2].indexOf(' – ') > 0) { // this is a non-ascii dash, we consider to be strong indication of artist/title separator
            split = m[2].split(' – ')
            album.artist = 'va'
            artist = split[0]
          }

          return {
            id: (album_id + 1) * 100 + (i),
            songnb: m[1],
            name: (split ? split[1] : m[2]).trim(),
            time: time,
            artist: artist,
            filename: track.filename
          }
        } else {
          // look for item without time
          m = l.match(/^(\d+)[ \t](.*)[ \t]-[ \t](.*)/)
          if (m && m.length === 4) {
            return {
              id: (album_id + 1) * 100 + (i),
              songnb: m[1],
              artist: m[2],
              name: m[3],
              time: '0:00',
              filename: track.filename
            }
          }
        }
      }).filter(v => !!v)

      if (tracks.length) {
        album.cue ||= [{}]
        album.cue[0].tracks = tracks
      }

      if (running_time_mode) {
        // times are all off by one - move them done
        album.cue[0].tracks.forEach((tr, i) => {
          if (i > 0)
            album.cue[0].tracks[i - 1].time = album.cue[0].tracks[i].time
        })
        // set the time on the last track
        if (track.time) {
          var t = track.time.split(':')
          if (t.length == 2) {
            t = string_array_to_time(t)
            album.cue[0].tracks[album.cue[0].tracks.length - 1].time = time_to_string(t - running_time)
          }
        }
      }
    }
  }
}

function base64Image (path) {
  try {
    const data = fs.readFileSync(path).toString('base64')
    return util.format("data:%s;base64,%s", mime.lookup(path), data)
  } catch(e) {
  }
}

function file_exists (filename) {
  try {
    const finfo = fs.statSync(filename)
      if (finfo) {
        return true
      }
  } catch(e) {
    if (e.code !== 'ENOENT') {
      throw e
    }
  }
  return false
}

module.exports = Scan
